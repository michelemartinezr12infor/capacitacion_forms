-- Ricardo Baja�a Galarza
-- Create table INFO_CLIENTES
create table INFO_CLIENTES
(
  id_cliente number not null,
  nombre     varchar2(250) not null,
  direccion  varchar2(500),
  telefono   varchar2(15) not null
)
;
-- Add comments to the table 
comment on table INFO_CLIENTES
  is 'Tabla de Clientes';
-- Add comments to the columns 
comment on column INFO_CLIENTES.id_cliente
  is 'Identificador de la tabla';
comment on column INFO_CLIENTES.nombre
  is 'Nombre del cliente';
comment on column INFO_CLIENTES.direccion
  is 'Direccion del cliente';
comment on column INFO_CLIENTES.telefono
  is 'Telefono del cliente';
  
-- Create/Recreate primary, unique and foreign key constraints 
alter table INFO_CLIENTES
  add constraint pk_cliente_id_cliente primary key (ID_CLIENTE);
  
  
  
-- Create table INFO_PROVEEDORES
create table INFO_PROVEEDORES
(
  id_proveedor number not null,
  nombre       varchar2(250) not null,
  direccion    varchar2(1000) not null,
  telefono     varchar2(15) not null
)
;
-- Add comments to the table 
comment on table INFO_PROVEEDORES
  is 'Tabla de proveedores';
-- Add comments to the columns 
comment on column INFO_PROVEEDORES.id_proveedor
  is 'Identificador proveedor';
comment on column INFO_PROVEEDORES.nombre
  is 'Nombre del proveedor';
comment on column INFO_PROVEEDORES.direccion
  is 'Direccion del proveedor';
comment on column INFO_PROVEEDORES.telefono
  is 'Telefono del proveedor';
  
-- Create/Recreate primary, unique and foreign key constraints 
alter table INFO_PROVEEDORES
  add constraint pk_proveedor_id_pro primary key (ID_PROVEEDOR);
  
  
  
-- Create table INFO_CATEGORIAS
create table INFO_CATEGORIAS
(
  id_categoria number not null,
  descripcion  varchar2(250) not null
)
;
-- Add comments to the table 
comment on table INFO_CATEGORIAS
  is 'Tabla de categorias';
-- Add comments to the columns 
comment on column INFO_CATEGORIAS.id_categoria
  is 'Identificador categoria';
comment on column INFO_CATEGORIAS.descripcion
  is 'Descripcion de la categoria';
-- Create/Recreate primary, unique and foreign key constraints 
alter table INFO_CATEGORIAS
  add constraint pk_id_categorias primary key (ID_CATEGORIA);
  

-- Create table INFO_PRODUCTOS
create table INFO_PRODUCTOS
(
  id_producto  number not null,
  descripcion  varchar2(250) not null,
  precio       number not null,
  categoria_id number not null,
  proveedor_id number not null
)
;
-- Add comments to the table 
comment on table INFO_PRODUCTOS
  is 'Tabla de productos';
-- Add comments to the columns 
comment on column INFO_PRODUCTOS.id_producto
  is 'Identificador del producto';
comment on column INFO_PRODUCTOS.descripcion
  is 'Descripcion del producto';
comment on column INFO_PRODUCTOS.precio
  is 'Precio del producto';
comment on column INFO_PRODUCTOS.categoria_id
  is 'id de la categoria del producto';
comment on column INFO_PRODUCTOS.proveedor_id
  is 'id de proveedor del producto';
-- Create/Recreate primary, unique and foreign key constraints 
alter table INFO_PRODUCTOS
  add constraint pk_id_producto primary key (ID_PRODUCTO);
alter table INFO_PRODUCTOS
  add constraint fk_prod_cat_id foreign key (CATEGORIA_ID)
  references info_categorias (ID_CATEGORIA);
alter table INFO_PRODUCTOS
  add constraint fk_prod_prov_id foreign key (PROVEEDOR_ID)
  references info_proveedores (ID_PROVEEDOR);
  
  
-- Create table INFO_FACTURAS
create table INFO_FACTURAS
(
  id_factura number not null,
  fecha      date not null,
  cliente_id number not null
)
;
-- Add comments to the table 
comment on table INFO_FACTURAS
  is 'Tabla de factura';
-- Add comments to the columns 
comment on column INFO_FACTURAS.id_factura
  is 'Identificador de la factura';
comment on column INFO_FACTURAS.fecha
  is 'Fecha de la factura';
comment on column INFO_FACTURAS.cliente_id
  is 'id del cliente de la factura';
-- Create/Recreate primary, unique and foreign key constraints 
alter table INFO_FACTURAS
  add constraint pk_id_factura primary key (ID_FACTURA);
alter table INFO_FACTURAS
  add constraint fk_id_cliente foreign key (CLIENTE_ID)
  references info_clientes (ID_CLIENTE);
  
  
-- Create table INFO_VENTAS
create table INFO_VENTAS
(
  id_venta    number not null,
  factura_id  number not null,
  producto_id number not null,
  cantidad    number not null
)
;
-- Add comments to the table 
comment on table INFO_VENTAS
  is 'Tabla de ventas';
-- Add comments to the columns 
comment on column INFO_VENTAS.id_venta
  is 'Identificador de venta';
comment on column INFO_VENTAS.factura_id
  is 'id de factura de la venta';
comment on column INFO_VENTAS.producto_id
  is 'id de producto de la venta';
comment on column INFO_VENTAS.cantidad
  is 'Cantidad';
-- Create/Recreate primary, unique and foreign key constraints 
alter table INFO_VENTAS
  add constraint pk_id_venta primary key (ID_VENTA);
alter table INFO_VENTAS
  add constraint fk_id_factura foreign key (FACTURA_ID)
  references info_facturas (ID_FACTURA);
alter table INFO_VENTAS
  add constraint fk_id_producto foreign key (PRODUCTO_ID)
  references info_productos (ID_PRODUCTO);
