--eliminacion de las tablas en este orden
--1. tabla INFO_VENTAS
drop table INFO_VENTAS;
--2. tabla INFO_PRODUCTOS
drop table INFO_PRODUCTOS;
--3. tabla INFO_FACTURAS
drop table INFO_FACTURAS;
--4. tabla INFO_CATEGORIAS
drop table INFO_CATEGORIAS;
--5. tabla INFO_PROVEEDORES
drop table INFO_PROVEEDORES;
--6. tabla INFO_CLIENTES
drop table INFO_CLIENTES;
