-- Crea tabla INFO_CLIENTES
create table INFO_CLIENTES
(
  pk_id_cliente NUMBER not null,
  nombre        VARCHAR2(250) not null,
  direccion     VARCHAR2(500),
  telefono      VARCHAR2(15) not null
);
-- Add comments to the table 
comment on table INFO_CLIENTES
  is 'Tabla de informacion de clientes';
-- Add comments to the columns 
comment on column INFO_CLIENTES.pk_id_cliente
  is 'Identificador unico de informacion de cliente';
comment on column INFO_CLIENTES.nombre
  is 'Nombre del cliente';
comment on column INFO_CLIENTES.direccion
  is 'Direccion del cliente';
comment on column INFO_CLIENTES.telefono
  is 'Telefono del cliente ';
-- Create/Recreate primary, unique and foreign key constraints 
alter table INFO_CLIENTES
  add constraint PK_ID_CLIENTE primary key (PK_ID_CLIENTE);
  
-- CREA TABLA INFO_FACTURA
create table INFO_FACTURAS
(
  pk_id_factura NUMBER not null,
  fecha         DATE not null,
  fk_cliente_id NUMBER not null
);
-- Add comments to the table 
comment on table INFO_FACTURAS
  is 'Tabla maestra de informacion de  factura de info_clientes';
-- Add comments to the columns 
comment on column INFO_FACTURAS.pk_id_factura
  is 'Indentificador unico de la factura';
comment on column INFO_FACTURAS.fecha
  is 'Fecha de informacion de factura';
comment on column INFO_FACTURAS.fk_cliente_id
  is 'Id de cliente de la tabla info_clientes';
-- Create/Recreate primary, unique and foreign key constraints 
alter table INFO_FACTURAS
  add constraint PK_ID_FACTURA primary key (PK_ID_FACTURA);
alter table INFO_FACTURAS
  add constraint FK_CLIENTE_ID foreign key (FK_CLIENTE_ID)
  references INFO_CLIENTES (PK_ID_CLIENTE);
  
-- CREA TABLA INFO_PROVEEDORES

create table INFO_PROVEEDORES
(
  pk_id_proveedor NUMBER not null,
  nombre          VARCHAR2(250) not null,
  direccion       VARCHAR2(1000) not null,
  telefono        VARCHAR2(15) not null
);
-- Add comments to the table 
comment on table INFO_PROVEEDORES
  is 'Tabla con informacion de proveedores de productos';
-- Add comments to the columns 
comment on column INFO_PROVEEDORES.pk_id_proveedor
  is 'Identificador unico de proveedor';
comment on column INFO_PROVEEDORES.nombre
  is 'Nombre de proveedor';
comment on column INFO_PROVEEDORES.direccion
  is 'Direccion de proveedor';
comment on column INFO_PROVEEDORES.telefono
  is 'Telefono de proveedor';
-- Create/Recreate primary, unique and foreign key constraints 
alter table INFO_PROVEEDORES
  add constraint PK_ID_PROVEEDOR primary key (PK_ID_PROVEEDOR);

-- Create table INFO_CATEGORIAS
create table INFO_CATEGORIAS
(
  pk_id_categoria NUMBER not null,
  descripcion     VARCHAR2(250) not null
);
-- Add comments to the table 
comment on table INFO_CATEGORIAS
  is 'Tabla con informacion de categorias';
-- Create/Recreate primary, unique and foreign key constraints 
alter table INFO_CATEGORIAS
  add constraint PK_ID_CATEGORIA primary key (PK_ID_CATEGORIA);

-- Create table INFO_PRODUCTOS
create table INFO_PRODUCTOS
(
  pk_id_producto  NUMBER not null,
  descripcion     VARCHAR2(250) not null,
  precio          NUMBER not null,
  fk_proveedor_id NUMBER not null,
  fk_categoria_id NUMBER not null
);
-- Add comments to the table 
comment on table INFO_PRODUCTOS
  is 'Tabla con informacion de productos ';
-- Add comments to the columns 
comment on column INFO_PRODUCTOS.pk_id_producto
  is 'Indentificador unico de prodcuto';
comment on column INFO_PRODUCTOS.descripcion
  is 'Descripcion de producto';
comment on column INFO_PRODUCTOS.precio
  is 'Precio de producto';
comment on column INFO_PRODUCTOS.fk_proveedor_id
  is 'ID de proveedor de la tabla INFO_PROVEEDORES';
comment on column INFO_PRODUCTOS.fk_categoria_id
  is 'ID categoria de la taba INFO_CATEGORIAS';
-- Create/Recreate primary, unique and foreign key constraints 
alter table INFO_PRODUCTOS
  add constraint PK_ID_PRODUCTO primary key (PK_ID_PRODUCTO);
alter table INFO_PRODUCTOS
  add constraint FK_ID_CATEGORIA foreign key (FK_CATEGORIA_ID)
  references INFO_CATEGORIAS (PK_ID_CATEGORIA);
alter table INFO_PRODUCTOS
  add constraint FK_ID_PROVEEDOR foreign key (FK_PROVEEDOR_ID)
  references INFO_PROVEEDORES (PK_ID_PROVEEDOR);

-- Create table INFO_VENTAS
create table INFO_VENTAS
(
  pk_id_venta    NUMBER not null,
  fk_factura_id  NUMBER not null,
  fk_producto_id NUMBER not null,
  cantidad       NUMBER not null
);
-- Add comments to the table 
comment on table INFO_VENTAS
  is 'Tabla de informacion de ventas de productos';
-- Add comments to the columns 
comment on column INFO_VENTAS.pk_id_venta
  is 'Identificador unico de venta';
comment on column INFO_VENTAS.fk_factura_id
  is 'ID factura de la tabla INFO_FACTURAS';
comment on column INFO_VENTAS.fk_producto_id
  is 'ID producto de la tabla INFO_PRODUCTOS';
comment on column INFO_VENTAS.cantidad
  is 'Cantidad de ventas';
-- Create/Recreate primary, unique and foreign key constraints 
alter table INFO_VENTAS
  add constraint PK_ID_VENTA primary key (PK_ID_VENTA);
alter table INFO_VENTAS
  add constraint FK_ID_FACTURA foreign key (FK_FACTURA_ID)
  references INFO_FACTURAS (PK_ID_FACTURA);
alter table INFO_VENTAS
  add constraint FK_ID_PRODUCTO foreign key (FK_PRODUCTO_ID)
  references INFO_PRODUCTOS (PK_ID_PRODUCTO);
